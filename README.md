# app-blueprints

## Status

Incomplete

## Purpose

This module's responsibilities are:

1. Accept a model, and return an object consisting of the generated controller functions. It is still the responsibility of the controller function to bind these too a route. 
2. Provide a function to bind the functions to the expected paths, and return a router.