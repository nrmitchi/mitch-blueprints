
module.exports = function (app, Model) {
  return {
    find    : require('./actions/find.js')(app, Model) ,
    findOfUser : require('./actions/findOfUser.js')(app, Model) ,
    findOne : require('./actions/findOne.js')(app, Model) ,
    create  : require('./actions/create.js')(app, Model) ,
    update  : require('./actions/update.js')(app, Model) ,
    destroy : require('./actions/destroy.js')(app, Model) ,

    loadMiddleware : require('./actions/loadOneMiddleware.js')(app, Model) ,

    permissionCheck : require('./actions/permissions.js')(app, Model) ,

    relations : require('./actions/relations.js')(app, Model),

    notImplemented : require('./actions/notImplemented.js')

  };
};
