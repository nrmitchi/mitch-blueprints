
var _ = require('lodash') ;
var url = require('url') ;
var qs = require('qs') ;
var inflection = require('inflection');

_.str = require('underscore.string') ;
_.mixin(_.str.exports());
_.str.include('Underscore.string', 'string');


module.exports = function buildPagination (req, pagination_type, count, searchObject, models) {
  /**
   * pagination_type must be either 'id' or 'offset'
   */

  var fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl;

  var meta = {
    total: count,
    next: '',
    prev: ''
  };

  var u = url.parse(fullUrl);
  var params = qs.parse(u.query || '');

  if (pagination_type == 'offset') {
    // Get last
    var offset = (searchObject.offset || 0);

    // Calculate next
    if (offset > count - (searchObject.limit || searchObject.per_page)) {
      // This is the last page
      meta.next = null;
    } else {
      if (req.query.page) {
        params.page = parseInt(req.query.page) + 1;
      } else {
        // Else we assume it's offset
        params.offset = parseInt(offset) + searchObject.limit;
      }

      u.query = qs.stringify(params);
      u.search = '?'+u.query;
      meta.next = u.format();
    }
    
    // Calculate previous
    if (offset === 0) {
      // This is already the first page
      meta.prev = null;
    } else {
      var prevPage = Math.max(parseInt(offset) - searchObject.limit, 0);

      if (req.query.page) {
        params.page = parseInt(req.query.page) - 1;
      } else {
        // Else we assume it's offset
        params.offset = parseInt(offset) - searchObject.limit;

        // Don't include the offset param if it's 0
        if (params.offset <= 0) {
          delete params.offset;
        }
      }

      u.query = qs.stringify(params);
      u.search = '?'+u.query;
      meta.prev = u.format();
    }
  } else if (pagination_type == 'id') {
    // This pagination type uses greater/less than an ID instead of using offsets.
    // Pro: Much much better db performance at high pages on long lists
    // Con: The 'total count' returned by the db would be wrong
    // Due to the con above, I'm ignoring for now
  }

  return meta;
};
