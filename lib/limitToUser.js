
var _ = require('lodash') ;

module.exports = function limitToAccount (app, Model, searchObject, account_id) {
  
  _.each(['account_id', 'user_id'], function (attr) {
    if (_.includes(_.keys(Model.attributes), attr)) {
      searchObject.where = searchObject.where || {};
      searchObject.where[attr] = account_id;
    }
  });

  return searchObject;
};
