
var _ = require('lodash') ;
var url = require('url') ;
var qs = require('qs') ;
var inflection = require('inflection');

_.str = require('underscore.string') ;
_.mixin(_.str.exports());
_.str.include('Underscore.string', 'string');

// Note: Currently doesn't support renaming attributes the way sequelize does
 // Perhaps I should check if the first character is '[', and if so, use 
 // JSON.parse
function normalizeQueryArray (aliasFilter) {
  // 'Borrowed' from Sails
  // Convert the string representation of the filter list to an Array. We
  // need this to provide flexibility in the request param. This way both
  // list string representations are supported:
  //   /model?populate=alias1,alias2,alias3
  //   /model?populate=[alias1,alias2,alias3]
  if (typeof aliasFilter === 'string') {
    aliasFilter = aliasFilter.replace(/\[|\]/g, '');
    aliasFilter = (aliasFilter) ? aliasFilter.split(',') : [];
  }

  return aliasFilter;
}

function processSorting (querySort, queryOrder) {

  var processedSort = [[querySort, queryOrder.toUpperCase()]];

  if (_.isArray(querySort)) {
    processedSort = _.map(querySort, function (sortable) {
      var attr = sortable[0];
      var order = sortable[1];

      if (_.isUndefined(order)) {
        // Then the order was not defined, we default to desc, unless a queryOrder was provided
        order = queryOrder;

        // It's also possible that the first param is actually an order not an attr, so confirm
        if (_.contains(['desc', 'asc'], attr.toLowerCase())) {
          order = attr;
          attr = id;
        }
      }
      return [attr, order.toUpperCase()];
    })
  }

  return processedSort;
}

module.exports = function buildSearchObject (app, Model, query) {
  // Extract relevant variables from query params; manage valid aliases
  // Todo: This should be changed to a class/model with appropriate validation baked in
  var queryWhere        = query.where ,
      queryAttributes   = query.attributes ,
      queryLimit        = query.limit || query.per_page ,
      queryOffset       = query.page ? (query.page - 1) * queryLimit : (query.offset || query.skip) ,
      querySort         = query.sort || query.sort_by || 'id',  // Todo: Parameterize what the default sort attr should be per model
      queryOrder        = query.order || 'desc',
      queryInclude      = query.include || query.populate ;

  // We have to use the ID always. We can't use anything else, because there could be multiple hits on any other key.
  // If we ended up with more than <page> elements with a given value for a key, we would not be able to page through them.
  // if (query.before) {
  //   queryWhere.id = {
  //     lt: res.query.before
  //   }
  // } else if (query.after) {  // Must be after
  //   queryWhere.id = {
  //     gt: res.query.before
  //   }
  // }

  var toIncludeRelations = _.compact(_.map(normalizeQueryArray(queryInclude || ''), function (model) {
    var classCase = _.classify(model);
    // console.log(model)
    // console.log(classCase)
    return {
      model: app.models[classCase],
      limit: 100 // Todo: A good way of parameterizing this somehow. Also figure out if this is doing anything
    };
  }));

  // Todo: Remove an toIncludeRelations with model = undefined
  toIncludeRelations = _.filter(toIncludeRelations, function (obj) {
    return !_.isUndefined(obj.model);
  });

  var toIncludeAttributes = _.compact(normalizeQueryArray(queryAttributes || ''));

  // Default and/or max limit
  // Todo: Right now I'm assuming limit is an int; this should be enforced
  queryLimit = queryLimit ? Math.min(queryLimit, 100) : 25;

  // Normalize sort/ordering input
  var processedSort = processSorting(querySort, queryOrder);

  // Todo: Normalize formats this could be in
  var searchObject = {
    where       : queryWhere ,
    attributes  : toIncludeAttributes.length ? toIncludeAttributes : undefined ,
    offset      : queryOffset ,
    limit       : queryLimit ,
    order       : processedSort ,
    include     : toIncludeRelations.length ? toIncludeRelations : [] // [{ all: true }] // Default for now, could change
  };

  // Remove attributes in request which do not exist on model
  if (searchObject.where) {
    searchObject.where = _.pick(searchObject.where, _.keys(Model.attributes));
    
    if (_.keys(searchObject.where).length === 0) {
      // Nothing left, so make this undefined so it's ignored
      searchObject.where = undefined;
    }
  }

  if (searchObject.attributes) {
    searchObject.attributes = _.filter(searchObject.attributes, function (i) {
      return _.keys(Model.attributes).indexOf(i) >= 0;
    });
    
    if (searchObject.attributes.length === 0) {
      // Nothing left, so make this undefined so it's ignored
      searchObject.attributes = undefined;
    }
  }

  return searchObject;
};
