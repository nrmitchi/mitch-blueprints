
var _ = require('lodash') ;
var mitch = require('mitch') ;
var inflection = require('inflection');
var buildPagination = require('../lib/buildPagination');

module.exports = function (app, modelName) {
  return function (relationModel, options) {

    options = options || {};

    var Blueprints  = require('mitch-blueprints')(app, relationModel);

    var router = app.Router();

    var model_name = _.underscored(modelName);
    var relation_name = _.underscored(relationModel);
    var relation_id = relation_name+'_id';

    router.param( relation_id , Blueprints.loadMiddleware );

    router.get('/', function getRelationModels (req, res, next) {

      var Model = app.models[relationModel];

      req[model_name]['get'+inflection.pluralize(relationModel)]({}, { raw: true })
        .then(function(models) {
          var response = {};
          response[inflection.pluralize(model_name)] = models;

          // Todo: When proper pagination is set up, re-add this
          // response.meta = buildPagination(req, 'offset', models.length, {}, models);
          res.json(response);
        })
        .catch( next );
    });

    router.post('/', function createRelationModel (req, res, next) {

      var Model = app.models[relationModel];

      var model = Model.build(req.body);

      model.save()
        .then(function () {
          return req[model_name]['add'+relationModel](model);
        })
        .then( function() {
          res.status(201).json(model);
          return null;
        })
        .catch( next );
    });

    router.post('/:'+relation_id, function addRelationModel (req, res, next) {
      req[model_name]['add'+relationModel](req[relation_name])
        .then(function() {
          res.status(200).json(req[relation_name]);
          return null;
        }).catch( next );
    });

    router.delete('/:'+relation_id, function addRelationModel (req, res, next) {
      req[model_name]['remove'+relationModel](req[relation_name])
        .then(function() {
          res.status(204).json({});
          return null;
        }).catch( next );
    });

    return router;

  };
};
