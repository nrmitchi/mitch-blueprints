
var _ = require('lodash') ;

module.exports = function (app, modelName) {

  var createModel = function createModel (req, res, next) {

    var Model = app.models[modelName];

    var model = Model.build(req.body);

    if (!_.isUndefined(model.dataValues.id)) {
      model.dataValues.id = undefined;
    }
    if (!_.isUndefined(model.dataValues.createdAt)) {
      model.dataValues.createdAt = undefined;
    }
    if (!_.isUndefined(model.dataValues.updatedAt)) {
      model.dataValues.updatedAt = undefined;
    }
    if (!_.isUndefined(model.dataValues.deletedAt)) {
      model.dataValues.deletedAt = undefined;
    }

    model.save()
      .then(function () {
        // Should give this the option to use a callback (could use for things like adding the new model to a relation)
        res.status(201).json(model);
        return null;
      })
      .catch( next );
  };

  return createModel;

};
