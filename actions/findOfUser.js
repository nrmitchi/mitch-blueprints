
var _ = require('lodash') ;
var inflection = require('inflection');

var buildPagination = require('../lib/buildPagination');
var buildSearchObject = require('../lib/buildSearchObject');

_.str = require('underscore.string') ;
_.mixin(_.str.exports());
_.str.include('Underscore.string', 'string');

// Note: Currently doesn't support renaming attributes the way sequelize does
 // Perhaps I should check if the first character is '[', and if so, use 
 // JSON.parse
function normalizeQueryArray (aliasFilter) {
  // 'Borrowed' from Sails
  // Convert the string representation of the filter list to an Array. We
  // need this to provide flexibility in the request param. This way both
  // list string representations are supported:
  //   /model?populate=alias1,alias2,alias3
  //   /model?populate=[alias1,alias2,alias3]
  if (typeof aliasFilter === 'string') {
    aliasFilter = aliasFilter.replace(/\[|\]/g, '');
    aliasFilter = (aliasFilter) ? aliasFilter.split(',') : [];
  }

  return aliasFilter;
}

module.exports = function (app, modelName) {

  var model_name = _.underscored(modelName);

  var findModelOfUser = function findModelOfUser (req, res, next) {
    
    var Model = app.models[modelName];

    var searchObject = buildSearchObject(app, Model, req.query);

    var plural = inflection.pluralize(Model.name)
    req.user['get'+plural](searchObject)
      .then( function (results) {

        // We want to be able to accept the response of find, or findAndCount.
        // This allows us to overwrite the get<Something> function on the user model
        // the return count values as well.
        var models;
        var count;

        if (!_.isUndefined(results.count)) {
          models = results.rows;
          count = results.count;
        } else {
          models = results;
          count = results.length;
        }

        var response = {};

        response[inflection.pluralize(_.underscored(modelName))] = models;
        response.meta = buildPagination(req, 'offset', count, searchObject, models);

        return res.json(response);
      })
      .catch( next );
  };

  return findModelOfUser;
};
