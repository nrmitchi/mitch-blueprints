
var _ = require('lodash') ;

_.str = require('underscore.string') ;
_.mixin(_.str.exports());
_.str.include('Underscore.string', 'string');

// Note: Currently doesn't support renaming attributes the way sequelize does
 // Perhaps I should check if the first character is '[', and if so, use 
 // JSON.parse
function normalizeQueryArray (aliasFilter) {
  // 'Borrowed' from Sails
  // Convert the string representation of the filter list to an Array. We
  // need this to provide flexibility in the request param. This way both
  // list string representations are supported:
  //   /model?populate=alias1,alias2,alias3
  //   /model?populate=[alias1,alias2,alias3]
  if (typeof aliasFilter === 'string') {
    aliasFilter = aliasFilter.replace(/\[|\]/g, '');
    aliasFilter = (aliasFilter) ? aliasFilter.split(',') : [];
  }

  return aliasFilter;
}

module.exports = function (app, modelName) {

  var model_name = _.underscored(modelName);
  
  var showModel = function showModel (req, res, next) {

    var Model = app.models[modelName];
    
    // Extract relevant variables from query params; manage valid aliases
    var queryAttributes   = req.query.attributes ,
        queryInclude      = req.query.include || req.query.populate ;

    var toIncludeRelations = _.compact(_.map(normalizeQueryArray(queryInclude || ''), function (model) {
      var classCase = _.classify(model);
      return app.models[classCase];
    }));

    var toIncludeAttributes = _.compact(normalizeQueryArray(queryAttributes || ''));

    // Todo: Normalize formats this could be in
    var searchObject = {
      where       : { id: req[model_name].id } ,
      attributes  : toIncludeAttributes.length ? toIncludeAttributes : undefined ,
      include     : toIncludeRelations.length ? toIncludeRelations : [] // Default for now, could change
    };

    // Remove attributes in request which do not exist on model
    if (searchObject.attributes) {
      searchObject.attributes = _.filter(searchObject.attributes, function (i) {
        return _.keys(Model.attributes).indexOf(i) >= 0;
      });

      if (searchObject.attributes.length === 0) {
        // Nothing left, so make this undefined so it's ignored
        searchObject.attributes = undefined;
      }
    }

    // Todo: Instead of refetching, I could just limit the attributes with _.pick()
    //       I would have to fetch included models still though
    Model.find(searchObject)
      .then( function (model) {
        return res.status(200).json(model);
      })
      .catch( next );

  };

  return showModel;
};
