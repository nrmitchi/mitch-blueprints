
var _ = require('lodash') ;

_.str = require('underscore.string') ;
_.mixin(_.str.exports());
_.str.include('Underscore.string', 'string');


module.exports = function (app, modelName) {

  var model_name = _.underscored(modelName);
  
  var updateModel = function updateModel (req, res, next) {

    var Model = app.models[modelName];

    req[model_name].updateAttributes(req.body, Model.attributeWhitelist)
      .then( function (model) {
        return res.status(200).json(model);
      })
      .catch( next );
  };

  return updateModel;
};
