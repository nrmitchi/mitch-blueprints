
var _ = require('lodash') ;

_.str = require('underscore.string') ;
_.mixin(_.str.exports());
_.str.include('Underscore.string', 'string');

module.exports = function (app, modelName) {

  var model_name = _.underscored(modelName);
  
  var loadModel = function loadModel (req, res, next, id) {

    var Model = app.models[modelName];

    Model.findById(id, { include: []})
      .then( function (model) {
        if (!model) {
          res.status(404).end();
          return null;
        } else {
          req[model_name] = model;
          next();
          return null;
        }
      })
      .catch( function (err) {
        res.status(500).json(err);
      });
  };

  return loadModel;
};
