
var _ = require('lodash') ;

_.str = require('underscore.string') ;
_.mixin(_.str.exports());
_.str.include('Underscore.string', 'string');

module.exports = function (app, Model) {
  return function permissionCheck (entity) {

    entity = _.underscored(entity);

    return function (req, res, next) {
      switch (req.method.toLowerCase()) {
        case 'get':
          if (req[entity]) {
            if (req.user && (req.user.id === req[entity].user_id)) {
              next();
            } else {
              res.status(403).json();
            }
          } else {
            next();
          }
          break;
        case 'post':
          next();
          break;
        case 'put':
          if (req[entity]) {
            if (req.user && (req.user.id === req[entity].user_id)) {
              next();
            } else {
              res.status(404).json();
            }
          } else {
            res.status(501).json();
          }
          break;
        case 'delete':
          if (req[entity]) {
            if (req.user && (req.user.id === req[entity].user_id)) {
              next();
            } else {
              res.status(404).json();
            }
          } else {
            res.status(501).json();
          }
          break;
        default:
          res.status(501).json();
          break;
      } // switch
        
    }; // function
  }; // function
}; // module.exports
