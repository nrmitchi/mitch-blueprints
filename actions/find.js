
var _ = require('lodash') ;
var url = require('url') ;
var qs = require('qs') ;
var inflection = require('inflection');

var buildPagination = require('../lib/buildPagination');
var buildSearchObject = require('../lib/buildSearchObject');
var limitToUser = require('../lib/limitToUser');

_.str = require('underscore.string') ;
_.mixin(_.str.exports());
_.str.include('Underscore.string', 'string');

module.exports = function (app, modelName) {

  var model_name = _.underscored(modelName);
  
  var findModel = function findModel (req, res, next) {

    var Model = app.models[modelName];

    var searchObject = buildSearchObject(app, Model, req.query);

    // If there is no user, and we got to this point, this must be a publicly available endpoint
    if (req.user) {
      searchObject = limitToUser(app, Model, searchObject, req.user.id);
    }

    // console.log(searchObject)
    Model.findAndCountAll(searchObject)
      .then( function (result) {
        var models = result.rows;
        var count = result.count;

        var response = {};

        response[inflection.pluralize(model_name)] = models;
        response.meta = buildPagination(req, 'offset', count, searchObject, models);

        return res.json(response);
      })  
      .catch( next );
  };

  return findModel;
};
