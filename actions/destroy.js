
var _ = require('lodash') ;

module.exports = function (app, modelName) {

  var model_name = _.underscored(modelName);
  
  var deleteModel = function deleteModel (req, res, next) {
    
    var Model = app.models[modelName];

    if (!req[model_name]) {
      // app.Logger.warn(':: '+Model.name+'::delete - no model loaded');
    }

    req[model_name].destroy()
      .then( function (err) {
        return res.status(204).json();
      })
      .catch( next );
  };

  return deleteModel;
};
