
var blueprints = require('..');
var request = require('supertest');
var should = require('should');
var async = require('async');
var _ = require('lodash');
var qs = require('qs');

describe('blueprints', function(){
  var app, Blueprints;

  before(function (done){
    app = require('./fixtures/app')();
    Blueprints = blueprints(app, 'Pet');

    app.param('id', Blueprints.loadMiddleware );

    app.syncModels()(done);

  });

  after(function (done) {
    async.eachSeries(_.keys(app.models), function (model, callback) {
      app.models[model].drop({cascade: true}).then(function () {
        callback.apply(arguments);
        return null;
      });
    }, function () {
      done();
    });
  });

  describe('relations', function() {
    var model;
    var childModel;

    before(function (done) {
      app.use('/:id/relation', Blueprints.relations('Leash') );
      done();
    });

    beforeEach(function (done) {
      // Should probably do this direct in DB instead of through post
      var pet = {
        name : 'tobi' , 
        type : 'dog' ,
        birthdate : '2015-01-01'
      };

      app.models.Pet.create(pet).then( function (pet) {
        model = pet;
        done();
        return null;
      });
    });

    describe('create', function () {
      it('create', function (done) {
        var leash = {
          length : 10 ,
          color  : 'red'
        };

        request(app)
          .post('/'+model.id+'/relation')
          .set('Content-Type', 'application/json')
          .send(leash)
          .expect(201)
          .expect('Content-Type', /json/)
          .expect(function (res) {
            res.body.should.be.instanceof(Object);
            res.body.length.should.equal(10);
            res.body.color.should.equal('red');
          })
          .end(done);
      });
    });
    describe('add', function () {
      var child;
      before(function (done) {
        var leash = {
          length : 10 ,
          color  : 'red'
        };

        app.models.Leash.create(leash).then( function (leash) {
          child = leash;
          done();
          return null;
        });
      });

      it('add existing model', function (done) {
        request(app)
          .post('/'+model.id+'/relation/'+child.id)
          .expect(200)
          .expect('Content-Type', /json/)
          .expect(function (res) {
            res.body.should.be.instanceof(Object);
            res.body.length.should.equal(10);
            res.body.color.should.equal('red');
          })
          .end(done);
      });
      it('add non-existing model', function (done) {
        child.destroy().then(function () {
          request(app)
            .post('/'+model.id+'/relation/'+child.id)
            .expect(404)
            // .expect('Content-Type', /json/)
            .expect(function (res) {
              res.body.should.be.instanceof(Object);
            })
            .end(done);
        });
      });
    });
    describe('remove', function () {
      var child;
      before(function (done) {
        var leash = {
          length : 10 ,
          color  : 'red'
        };

        app.models.Leash.create(leash).then( function (leash) {
          child = leash;
          return model.addLeash(leash);
        }).then(function (leash) {
          done();
          return null;
        });
      });

      it('remove', function (done) {
        request(app)
          .delete('/'+model.id+'/relation/'+child.id)
          .expect(204)
          // .expect('Content-Type', /json/)
          .expect(function (res) {
            res.body.should.be.instanceof(Object);
          })
          .end(done);
      });
      it('remove non-related model', function (done) {
        model.removeLeash(child).then( function () {
          request(app)
            .delete('/'+model.id+'/relation/'+child.id)
            .expect(204)
            // .expect('Content-Type', /json/)
            .expect(function (res) {
              res.body.should.be.instanceof(Object);
            })
            .end(done);
        });
      });
    });
  });
});
