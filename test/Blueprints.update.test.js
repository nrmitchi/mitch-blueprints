
var blueprints = require('..');
var request = require('supertest');
var should = require('should');
var async = require('async');
var _ = require('lodash');
var qs = require('qs');

describe('blueprints', function(){
  var app, Blueprints;

  before(function (done){
    app = require('./fixtures/app')();
    Blueprints = blueprints(app, 'Pet');

    app.param('id', Blueprints.loadMiddleware );

    app.syncModels()(done);

  });

  after(function (done) {
    async.eachSeries(_.keys(app.models), function (model, callback) {
      app.models[model].drop({cascade: true}).then(function () {
        callback.apply(arguments);
        return null;
      });
    }, function () {
      done();
    });
  });

  describe('update', function() {
    var model;
    before(function (done) {
      app.put('/:id', Blueprints.update );
      done();
    });
    beforeEach(function (done) {
      // Should probably do this direct in DB instead of through post
      var pet = {
        name : 'tobi' , 
        type : 'dog' ,
        birthdate : '2015-01-01'
      };

      app.models.Pet.create(pet).then( function (pet) {
        model = pet;
        done();
        return null;
      });

    });
    afterEach(function (done) {
      // Possibly delete the model if there ends up being any constraint violations
      done();
    });

    it('update', function (done) {
      request(app)
        .put('/'+model.id)
        .set('Content-Type', 'application/json')
        .send({
          type: 'cat'
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .expect(function (res) {
          res.body.should.be.instanceof(Object);
          res.body.id.should.equal(model.id);
          res.body.name.should.equal('tobi');
          res.body.type.should.equal('cat');
        })
        .end(done);
    });
    it('extra attributes should be ignored', function (done) {
      request(app)
        .put('/'+model.id)
        .set('Content-Type', 'application/json')
        .send({
          type: 'cat',
          unknown: true
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .expect(function (res) {
          res.body.should.be.instanceof(Object);
          res.body.id.should.equal(model.id);
          res.body.name.should.equal('tobi');
          res.body.type.should.equal('cat');
          should.not.exist(res.body.unknown);
        })
        .end(done);
    });

    it('included ID should be ignored', function (done) {
      request(app)
        .put('/'+model.id)
        .set('Content-Type', 'application/json')
        .send({
          id: model.id + 1,
          type: 'cat'
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .expect(function (res) {
          res.body.should.be.instanceof(Object);
          res.body.id.should.equal(model.id);
          res.body.type.should.equal('cat');
        })
        .end(done);
    });
    it('included default timestamps should be ignored', function (done) {
      request(app)
        .put('/'+model.id)
        .set('Content-Type', 'application/json')
        .send({
          id: model.id + 1,
          type: 'cat',
          createdAt: new Date('2015-01-01'),
          updatedAt: new Date('2015-01-01'),
          deletedAt: new Date('2015-01-01')
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .expect(function (res) {
          res.body.should.be.instanceof(Object);
          res.body.id.should.equal(model.id);
          res.body.type.should.equal('cat');
          (Date(res.body.createdAt)).should.equal(Date(model.createdAt));
          (Date(res.body.updatedAt) >= Date(model.updatedAt)).should.be.true;
          (Date(res.body.deletedAt)).should.equal(Date(model.deletedAt));
        })
        .end(done);
    });

    it('validation errors should return a 400 (with descriptive error)');
    it('database constraint errors should return a 400 (with descriptive error)');
  });
});
