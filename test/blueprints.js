
var blueprints = require('..');
var request = require('supertest');
var should = require('should');

describe('blueprints', function(){
  var app, Blueprints;

  beforeEach(function(done){
    app = require('./fixtures/app')();
    Blueprints = blueprints(app, 'Pet');

    done();
  });

  it('should export a function', function(done){
    should(typeof blueprints).equal('function');
    done();
  });

  it('should include all routes', function(){
    var Blueprints = blueprints(app, 'Pet');
    Blueprints.should.have.property('find');
    Blueprints.should.have.property('findOne');
    Blueprints.should.have.property('create');
    Blueprints.should.have.property('update');
    Blueprints.should.have.property('destroy');
    Blueprints.should.have.property('loadMiddleware');
    Blueprints.should.have.property('notImplemented');

    Blueprints.should.have.property('permissionCheck');

    Blueprints.should.have.property('relations');
  });

});
