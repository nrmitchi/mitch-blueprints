
var blueprints = require('..');
var request = require('supertest');
var should = require('should');
var async = require('async');
var _ = require('lodash');
var qs = require('qs');

describe('blueprints', function(){
  var app, Blueprints;

  before(function (done){
    app = require('./fixtures/app')();
    Blueprints = blueprints(app, 'Pet');

    app.param('id', Blueprints.loadMiddleware );

    app.syncModels()(done);

  });

  after(function (done) {
    async.eachSeries(_.keys(app.models), function (model, callback) {
      app.models[model].drop({cascade: true}).then(function () {
        callback.apply(arguments);
        return null;
      });
    }, function () {
      done();
    });
  });

  describe('findOne', function() {
    var model;
    before(function (done) {
      app.get('/:id', Blueprints.findOne );
      done();
    });
    beforeEach(function (done) {
      // Should probably do this direct in DB instead of through post
      var pet = {
        name : 'tobi' , 
        type : 'dog' ,
        birthdate : '2015-01-01'
      };

      app.models.Pet.create(pet).then( function (pet) {
        model = pet;
        done();
        return null;
      });
    });
    afterEach(function (done) {
      // Possibly delete the model if there ends up being any constraint violations
      done();
    });

    it('exists', function (done) {
      request(app)
        .get('/'+model.id)
        .expect(200)
        .expect('Content-Type', /json/)
        .expect(function (res) {
          res.body.should.be.instanceof(Object);
          res.body.id.should.equal(model.id);
          res.body.name.should.equal('tobi');
          res.body.type.should.equal('dog');
          res.body.birthdate.should.equal('2015-01-01T00:00:00.000Z');
        })
        .end(done);
    });
    it('does not exist', function (done) {

      model.destroy().then(function () {
        request(app)
          .get('/'+model.id)
          .expect(404)
          .end(done);
      });
    });

    it('only one attribute', function (done) {
      request(app)
        .get('/'+model.id+'?attributes=name')
        .expect(200)
        .expect('Content-Type', /json/)
        .expect(function (res) {
          res.body.should.be.instanceof(Object);
          res.body.should.have.keys('name');
          res.body.name.should.equal(model.name);
        })
        .end(done);
    });
    it('multiple attributes', function (done) {
      request(app)
        .get('/'+model.id+'?attributes=name,type')
        .expect(200)
        .expect('Content-Type', /json/)
        .expect(function (res) {
          res.body.should.be.instanceof(Object);
          res.body.should.have.keys('name', 'type');
          res.body.name.should.equal(model.name);
          res.body.type.should.equal(model.type);
        })
        .end(done);
    });
  });

});
