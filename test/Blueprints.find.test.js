
var blueprints = require('..');
var request = require('supertest');
var should = require('should');
var async = require('async');
var _ = require('lodash');
var qs = require('qs');

describe('blueprints', function(){
  var app, Blueprints;

  before(function (done){
    app = require('./fixtures/app')();
    Blueprints = blueprints(app, 'Pet');

    app.param('id', Blueprints.loadMiddleware );

    app.syncModels()(done);

  });

  after(function (done) {
    async.eachSeries(_.keys(app.models), function (model, callback) {
      app.models[model].drop({cascade: true}).then(function () {
        callback.apply(arguments);
        return null;
      });
    }, function () {
      done();
    });
  });

  describe('find', function() {
    var models = [];
    before(function (done) {
      app.get('/', Blueprints.find );
      done();
    });
    beforeEach( function (done) {
      // Create some fake models
      async.eachSeries(_.range(10), function(i, callback) {
        var pet = {
          name : 'tobi-'+(i%2) , 
          type : 'dog-'+(i%3) ,
          birthdate : '2015-01-01'
        };

        app.models.Pet.create(pet).then( function (pet) {
          models.push(pet);
          callback();
          return null;
        });
      }, done);
    });
    afterEach( function (done) {
      async.each(models, function(model, callback) {
        model.destroy().then(function () {
          callback();
          return null;
        });
      }, done);
    });

    it('entire model', function (done) {
      request(app)
        .get('/')
        .expect(200)
        .expect('Content-Type', /json/)
        .expect(function (res) {
          res.body.should.be.instanceof(Object);
          res.body.pets.should.be.instanceof(Array);

          // Since we will return in default desc by id, and our fixture models
          // were added asc; we need to resort our fixtures before comparing
          var expectedModels = _.sortBy(models, function(x) { return -x.id; });

          res.body.pets.forEach(function (x, i) {
            x.should.be.instanceof(Object);
            x.name.should.equal(expectedModels[i].name);
            x.type.should.equal(expectedModels[i].type);
          });
        })
        .end(done);
    });
    describe('where', function () {
      it('attribute', function (done) {
        request(app)
          .get('/?where[type]='+models[0].type)
          .expect(200)
          .expect('Content-Type', /json/)
          .expect(function (res) {
            res.body.should.be.instanceof(Object);
            res.body.pets.should.be.instanceof(Array);
            res.body.pets.forEach(function (x) {
              x.should.be.instanceof(Object);
              x.type.should.equal(models[0].type);
            });
          })
          .end(done);
      });
      it('multiple attributes', function (done) {
        request(app)
          .get('/?where[name]='+models[0].name+'&where[type]='+models[0].type)
          .expect(200)
          .expect('Content-Type', /json/)
          .expect(function (res) {
            res.body.should.be.instanceof(Object);
            res.body.pets.should.be.instanceof(Array);
            res.body.pets.forEach(function (x) {
              x.should.be.instanceof(Object);
              x.name.should.equal(models[0].name);
              x.type.should.equal(models[0].type);
            });
          })
          .end(done);
      });
      it('non-existent attributes are ignored', function (done) {
        request(app)
          .get('/?where[name]='+models[0].name+'&where[unknown]=1')
          .expect(200)
          .expect('Content-Type', /json/)
          .expect(function (res) {
            res.body.should.be.instanceof(Object);
            res.body.pets.should.be.instanceof(Array);
            res.body.pets.forEach(function (x) {
              x.should.be.instanceof(Object);
              x.name.should.equal(models[0].name);
            });
          })
          .end(done);
      });
    });

    // -------------------------------------------------------
    //      These suites should be in findOne as well

    describe('attributes', function () {
      it('only one attribute', function (done) {
        request(app)
          .get('/?attributes=name')
          .expect(200)
          .expect('Content-Type', /json/)
          .expect(function (res) {
            res.body.should.be.instanceof(Object);
            res.body.pets.should.be.instanceof(Array);
            res.body.pets.forEach(function (x) {
              x.should.be.instanceof(Object);
              x.should.have.keys('name');
            });
          })
          .end(done);
      });
      it('multiple attributes', function (done) {
        request(app)
          .get('/?attributes=name,type')
          .expect(200)
          .expect('Content-Type', /json/)
          .expect(function (res) {
            res.body.should.be.instanceof(Object);
            res.body.pets.should.be.instanceof(Array);
            res.body.pets.forEach(function (x) {
              x.should.be.instanceof(Object);
              x.should.have.keys('name', 'type');
            });
          })
          .end(done);
      });
      it('non-existent attributes are ignored', function (done) {
        request(app)
          .get('/?attributes=name,notreal')
          .expect(200)
          .expect('Content-Type', /json/)
          .expect(function (res) {
            res.body.should.be.instanceof(Object);
            res.body.pets.should.be.instanceof(Array);
            res.body.pets.forEach(function (x) {
              x.should.be.instanceof(Object);
              x.should.have.keys('name');
            });
          })
          .end(done);
      });
    });
    describe('include', function () {
      it('does not include by default', function (done) {
        var expectedKeys = [
          'id', 'name', 'type', 'birthdate', 
          'createdAt', 'updatedAt', 'deletedAt'
          ];

        request(app)
          .get('/')
          .expect(200)
          .expect('Content-Type', /json/)
          .expect(function (res) {
            res.body.should.be.instanceof(Object);
            res.body.pets.should.be.instanceof(Array);
            res.body.pets.forEach(function (model) {
              model.should.have.keys(expectedKeys);
            });
          })
          .end(done);
      });

      it('includes when requested', function (done) {
        request(app)
          .get('/?include=Leash')
          .expect(200)
          .expect('Content-Type', /json/)
          .expect(function (res) {

            res.body.should.be.instanceof(Object);
            res.body.pets.should.be.instanceof(Array);
            res.body.pets.forEach(function (model) {
              model.Leashes.should.exist;
              model.Leashes.should.be.instanceof(Array);
            });
          })
          .end(done);
      });
    });
    // -------------------------------------------------------

    describe('ordering', function () {
      it('by name', function (done) {
        var sortString = qs.stringify({sort: 'name'});

        request(app)
          .get('/?'+sortString)
          .expect(200)
          .expect('Content-Type', /json/)
          .expect(function (res) {
            res.body.should.be.instanceof(Object);
            res.body.pets.should.be.instanceof(Array);
            res.body.pets.length.should.equal(10);
            _.range(1, 10).forEach( function (i) {
              (res.body.pets[i].name <= res.body.pets[i-1].name).should.be.true;
            });
          })
          .end(done);
      });
      it('by type', function (done) {
        var sortString = qs.stringify({sort: [['name', 'ASC']]});

        request(app)
          .get('/?'+sortString)
          .expect(200)
          .expect('Content-Type', /json/)
          .expect(function (res) {
            res.body.should.be.instanceof(Object);
            res.body.pets.should.be.instanceof(Array);
            res.body.pets.length.should.equal(10);
            _.range(1, 10).forEach( function (i) {
              (res.body.pets[i].name >= res.body.pets[i-1].name).should.be.true;
            });
          })
          .end(done);
      });
    });
    describe('limit/offset', function () {

      it('limit', function (done) {
        request(app)
          .get('/?limit=5')
          .expect(200)
          .expect('Content-Type', /json/)
          .expect(function (res) {
            res.body.should.be.instanceof(Object);
            res.body.pets.length.should.equal(5);
          })
          .end(done);
      });
      
      it('offset', function (done) {
        request(app)
          .get('/?offset=2')
          .expect(200)
          .expect('Content-Type', /json/)
          .expect(function (res) {
            res.body.should.be.instanceof(Object);
            res.body.pets.should.be.instanceof(Array);
            res.body.pets.length.should.equal(8);
            res.body.pets.forEach(function (model) {
              model.id.should.be.greaterThan(2); // 1 and 2 should have been skipped
            });
          })
          .end(done);
      });
      it('offset with limit', function (done) {
        request(app)
          .get('/?limit=5&offset=2')
          .expect(200)
          .expect('Content-Type', /json/)
          .expect(function (res) {
            res.body.should.be.instanceof(Object);
            res.body.pets.should.be.instanceof(Array);
            res.body.pets.length.should.equal(5);
            res.body.pets.forEach(function (model) {
              model.id.should.be.greaterThan(2); // 1 and 2 should have been skipped
            });
          })
          .end(done);
      });
      
      describe('defaults/maximums', function () {
        var modelsForLimit = [];

        before( function (done) {
          // Create a bunch of models so we can test limits
          async.eachSeries(_.range(105), function(i, callback) {
            var pet = {
              name : 'tobi-'+(i%25) , 
              type : 'dog-'+(i%5) ,
              birthdate : '2015-01-01'
            };

            app.models.Pet.create(pet).then( function (pet) {
              modelsForLimit.push(pet);
              callback();
              return null;
            });
          }, done);
        });
        after( function (done) {
          async.each(modelsForLimit, function(model, callback) {
            model.destroy().then(function () {
              callback();
            });
          }, done);
        });

        it('limit default of 25', function (done) {
          request(app)
            .get('/')
            .expect(200)
            .expect('Content-Type', /json/)
            .expect(function (res) {
              res.body.should.be.instanceof(Object);
              res.body.pets.should.be.instanceof(Array);
              res.body.pets.length.should.not.be.greaterThan(25);
            })
            .end(done);
        });
        it('limit max of 100', function (done) {
          request(app)
            .get('/?limit=125')
            .expect(200)
            .expect('Content-Type', /json/)
            .expect(function (res) {
              res.body.should.be.instanceof(Object);
              res.body.pets.should.be.instanceof(Array);
              res.body.pets.length.should.not.be.greaterThan(100);
            })
            .end(done);
        });
      });
    });
    describe('meta', function () {
      var modelsForLimit = [];
      before( function (done) {
        // Create a bunch of models so we can test counts
        async.each(_.range(105), function(i, callback) {
          var pet = {
            name : 'tobi-'+(i%25) , 
            type : 'dog-'+(i%5) ,
            birthdate : '2015-01-01'
          };

          app.models.Pet.create(pet).then( function (pet) {
            modelsForLimit.push(pet);
            callback();
            return null;
          });
        }, done);
      });
      after( function (done) {
        async.each(modelsForLimit, function(model, callback) {
          model.destroy().then(function () {
            callback();
            return null;
          });
        }, done);
      });

      it('includes count', function (done) {
        request(app)
          .get('/')
          .expect(200)
          .expect('Content-Type', /json/)
          .expect(function (res) {
            res.body.should.be.instanceof(Object);
            res.body.meta.should.be.instanceof(Object);
            res.body.meta.total.should.equal(115);
          })
          .end(done);
      });

      it('includes next', function (done) {
        request(app)
          .get('/')
          .expect(200)
          .expect('Content-Type', /json/)
          .expect(function (res) {
            res.body.should.be.instanceof(Object);
            res.body.meta.should.be.instanceof(Object);
            res.body.meta.next.should.be.instanceof(String);
          })
          .end(done);
      });

      it('next null on last page', function (done) {
        request(app)
          .get('/?offset=125')
          .expect(200)
          .expect('Content-Type', /json/)
          .expect(function (res) {
            res.body.should.be.instanceof(Object);
            res.body.meta.should.be.instanceof(Object);
            should.equal(res.body.meta.next, null);
          })
          .end(done);
      });

      it('includes prev', function (done) {
        request(app)
          .get('/?offset=100')
          .expect(200)
          .expect('Content-Type', /json/)
          .expect(function (res) {
            res.body.should.be.instanceof(Object);
            res.body.meta.should.be.instanceof(Object);
            res.body.meta.prev.should.be.instanceof(String);
          })
          .end(done);
      });

      it('prev null on first page', function (done) {
        request(app)
          .get('/')
          .expect(200)
          .expect('Content-Type', /json/)
          .expect(function (res) {
            res.body.should.be.instanceof(Object);
            res.body.meta.should.be.instanceof(Object);
            should.equal(res.body.meta.prev, null);
          })
          .end(done);
      });

      it('maintains limit', function (done) {
        request(app)
          .get('/?offset=50&limit=25')
          .expect(200)
          .expect('Content-Type', /json/)
          .expect(function (res) {
            res.body.should.be.instanceof(Object);
            res.body.meta.should.be.instanceof(Object);
            
            res.body.meta.next.should.be.instanceof(String);
            res.body.meta.prev.should.be.instanceof(String);

            (res.body.meta.next.indexOf('limit')).should.not.equal(-1);
            (res.body.meta.prev.indexOf('limit')).should.not.equal(-1);
          })
          .end(done);
      });
    });
  });

});
