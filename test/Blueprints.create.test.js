
var blueprints = require('..');
var request = require('supertest');
var should = require('should');
var async = require('async');
var _ = require('lodash');
var qs = require('qs');

describe('blueprints', function(){
  var app, Blueprints;

  before(function (done){
    app = require('./fixtures/app')();
    Blueprints = blueprints(app, 'Pet');

    app.param('id', Blueprints.loadMiddleware );

    app.syncModels()(done);

  });

  after(function (done) {
    async.eachSeries(_.keys(app.models), function (model, callback) {
      app.models[model].drop({cascade: true}).then(function () {
        callback.apply(arguments);
        return null;
      });
    }, function () {
      done();
    });
  });

  describe('create', function() {
    before(function (done) {
      app.post('/', Blueprints.create );
      done();
    });

    it('valid', function (done) {
      var pet = {
        name : 'tobi' , 
        type : 'dog' ,
        birthdate : '2015-01-01'
      };

      request(app)
        .post('/')
        .set('Content-Type', 'application/json')
        .send(pet)
        .expect(201)
        .expect('Content-Type', /json/)
        .expect(function (res) {
          res.body.should.be.instanceof(Object);
          res.body.name.should.equal('tobi');
          res.body.type.should.equal('dog');
          res.body.birthdate.should.equal('2015-01-01T00:00:00.000Z');
        })
        .end(done);
    });

    it('extra attributes should be ignored', function (done) {
      var pet = {
        name : 'tobi' , 
        type : 'dog' ,
        birthdate : '2015-01-01',
        unknown: true
      };

      request(app)
        .post('/')
        .set('Content-Type', 'application/json')
        .send(pet)
        .expect(201)
        .expect('Content-Type', /json/)
        .expect(function (res) {
          res.body.should.be.instanceof(Object);
          res.body.name.should.equal('tobi');
          res.body.type.should.equal('dog');
          res.body.birthdate.should.equal('2015-01-01T00:00:00.000Z');
        })
        .end(done);
    });

    it('included ID should be ignored', function (done) {
      var pet = {
        id   : 10000 ,
        name : 'tobi' , 
        type : 'dog' ,
        birthdate : '2015-01-01'
      };

      request(app)
        .post('/')
        .set('Content-Type', 'application/json')
        .send(pet)
        .expect(201)
        .expect('Content-Type', /json/)
        .expect(function (res) {
          res.body.should.be.instanceof(Object);
          res.body.id.should.not.equal(pet.id);
        })
        .end(done);
    });

    it('included default timestamps should be ignored', function (done) {
      var pet = {
        name : 'tobi' , 
        type : 'dog' ,
        birthdate : '2015-01-01',
        createdAt: new Date('2015-01-01'),
        updatedAt: new Date('2015-01-02'),
        deletedAt: new Date('2015-01-01')
      };

      request(app)
        .post('/')
        .set('Content-Type', 'application/json')
        .send(pet)
        .expect(201)
        .expect('Content-Type', /json/)
        .expect(function (res) {
          res.body.should.be.instanceof(Object);
          var createdAt = (new Date(res.body.createdAt)).getTime();
          var updatedAt = (new Date(res.body.updatedAt)).getTime();

          createdAt.should.equal(updatedAt);
          should.equal(res.body.deletedAt, null);
        })
        .end(done);
    });

    it('non-required missing attributes should be okay', function (done) {
      var pet = {
        name : 'tobi' , 
        type : 'dog'
      };

      request(app)
        .post('/')
        .set('Content-Type', 'application/json')
        .send(pet)
        .expect(201)
        .expect('Content-Type', /json/)
        .expect(function (res) {
          res.body.should.be.instanceof(Object);
          res.body.name.should.equal('tobi');
          res.body.type.should.equal('dog');
          should.equal(res.body.birthdate, null);
        })
        .end(done);
    });
    it('required missing attributes should fail (with descriptive error)');
    it('validation errors should return a 400 (with descriptive error)');
    it('database constraint errors should return a 400 (with descriptive error)');
  });
});
