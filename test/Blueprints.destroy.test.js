
var blueprints = require('..');
var request = require('supertest');
var should = require('should');
var async = require('async');
var _ = require('lodash');
var qs = require('qs');

describe('blueprints', function(){
  var app, Blueprints;

  before(function (done){
    app = require('./fixtures/app')();
    Blueprints = blueprints(app, 'Pet');

    app.param('id', Blueprints.loadMiddleware );

    app.syncModels()(done);

  });

  after(function (done) {
    async.eachSeries(_.keys(app.models), function (model, callback) {
      app.models[model].drop({cascade: true}).then(function () {
        callback.apply(arguments);
        return null;
      });
    }, function () {
      done();
    });
  });

  describe('destroy', function() {

    var model;

    before(function(done) {
      app.delete('/:id', Blueprints.destroy );
      done();
    });
    beforeEach(function (done) {
      // Should probably do this direct in DB instead of through post
      var pet = {
        name : 'tobi' , 
        type : 'dog' ,
        birthdate : '2015-01-01'
      };

      app.models.Pet.create(pet).then( function (pet) {
        model = pet;
        done();
        return null;
      });
    });

    it('exists', function (done) {
      request(app)
        .delete('/'+model.id)
        // .set('Content-Type', 'application/json')
        .expect(204)
        // .expect('Content-Type', /json/)
        .expect(function (res) {
          res.body.should.be.instanceof(Object);
          Object.keys(res.body).length.should.equal(0);
        })
        .end(done);
    });
    it('does not exist', function (done) {
      model.destroy().then( function () {
        request(app)
          .delete('/'+model.id)
          // .set('Content-Type', 'application/json')
          .expect(404)
          // .expect('Content-Type', /json/)
          .expect(function (res) {
            res.body.should.be.instanceof(Object);
            Object.keys(res.body).length.should.equal(0);
          })
          .end(done);
      });
    });

    it('should do soft delete (with deletedAt set)', function (done) {
      request(app)
        .delete('/'+model.id)
        // .set('Content-Type', 'application/json')
        .expect(204)
        // .expect('Content-Type', /json/)
        .expect(function (res) {
          res.body.should.be.instanceof(Object);
          Object.keys(res.body).length.should.equal(0);
        })
        .end(function (err) {
          if (err) throw err;

          app.models.Pet.find({
              where: model.id,
              paranoid: false
            })
            .then(function (deleted) {
              (deleted.deletedAt === null).should.be.false;
              done();
            });
        });
    });
  });
});
