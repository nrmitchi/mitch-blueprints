
var _ = require('lodash');
var mitch = require('mitch');
var Sequelize = require('sequelize');

module.exports = function createApp () {

  var app = mitch();

  app.config = {};
  _.extend(app.config, {
    sequelize: {
      host: 'localhost',
      username: 'ubuntu',
      password: '',
      database: 'circle_test',
      dialect: 'postgres',
      pool: {
        max: 5,
        min: 0,
        idle: 10000
      },
      logging: false
    },
    globals: {}
  });

  app.loadMiddleware();

  app._models.push({
    module  : 'Pets' ,
    Factory : require('./models/pet')(app)
  });
  app._models.push({
    module  : 'Leashes' ,
    Factory : require('./models/leash')(app)
  });

  app.loadModels();

  app.use(function (err, res, req, next) {
    res.json(err)
  });

  return app;

};
