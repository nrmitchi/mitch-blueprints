
var debug = require('debug')('example:leashes:model');

module.exports = function (app) {
  return function(sequelize, DataTypes) {
    var Leash = sequelize.define('Leash', {  
      length : {
        type : DataTypes.INTEGER ,
        allowNull : false
      } , 
      color : {
        type : DataTypes.STRING ,
        allowNull : false
      }
    }, {
      paranoid: true ,
      tableName: 'leashes' ,
      comment: 'Leashes database' ,
      associate: function(models) {
        models.Pet.hasMany(Leash, { foreignKey: 'pet_id' });
        Leash.belongsTo(models.Pet, { foreignKey: 'pet_id' });
      }
    });

    return Leash;
  };
};
