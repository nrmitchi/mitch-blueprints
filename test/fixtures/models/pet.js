
var debug = require('debug')('example:pets:model');

module.exports = function (app) {
  return function(sequelize, DataTypes) {
    var Pet = sequelize.define('Pet', {  
      name : {
        type : DataTypes.STRING ,
        allowNull : false 
      } , 
      type : {
        type : DataTypes.STRING ,
        allowNull : false
      } ,
      birthdate : {
        type : DataTypes.DATE
      }
    }, {
      paranoid: true
    });

    return Pet;
  };
};
